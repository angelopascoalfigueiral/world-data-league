# World Data League

# This is the repository for Money House group in the World Data League competition. The aim of the challenges is to leverage data to propose solution to real social issues.

# The submission file can be found under each stage, under the name "Submission".